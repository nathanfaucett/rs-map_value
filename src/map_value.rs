pub trait MapValue: Sized {
    /// # Example
    /// ```
    /// use map_value::MapValue;
    ///
    /// assert_eq!(10.map_ref(|x| x + 10), 20);
    /// ```
    #[inline(always)]
    fn map_ref<F, T>(&self, mut f: F) -> T
    where
        F: FnMut(&Self) -> T,
    {
        f(self)
    }
    /// # Example
    /// ```
    /// use map_value::MapValue;
    ///
    /// let mut x = 10;
    /// let y = x.map_mut(|x| { *x = *x + 10; *x });
    /// assert_eq!(x, 20);
    /// assert_eq!(y, 20);
    /// ```
    #[inline(always)]
    fn map_mut<F, T>(&mut self, mut f: F) -> T
    where
        F: FnMut(&mut Self) -> T,
    {
        f(self)
    }
    /// # Example
    /// ```
    /// use map_value::MapValue;
    ///
    /// assert_eq!(10.map_take(|x| x + 10), 20);
    /// ```
    #[inline(always)]
    fn map_take<F, T>(self, mut f: F) -> T
    where
        F: FnMut(Self) -> T,
    {
        f(self)
    }
}

impl<T> MapValue for T {}
