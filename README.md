# rs-map_value

=====

map value with map_ref, map_mut, and map_take

```rust
use map_value::MapValue;

let x = 10;
let y = x.map_ref(|x| x + 10);

assert!(x, 10);
assert!(y, 20);
```
